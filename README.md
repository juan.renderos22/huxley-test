# Huxley TEST FizzBuzz
Spring boot application

Prerequisites to test application

1) Installed Java Jdk 11 or higher
2) Installed maven
3) Installed any IDE
4) Git 

Set up the project

1) Clone the project
2) Install Dependencies with Maven (mvn clean install)
3) Import project to IDE
4) Run docker compose, or star spring boot application.