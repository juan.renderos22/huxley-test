package com.huxley.controller;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doReturn;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.github.javafaker.Faker;
import com.huxley.controller.response.FizzBuzzData;
import com.huxley.controller.response.FizzBuzzModel;
import com.huxley.service.FizzBuzzService;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultActions;

/**
 * Test the GET Method expose on FizzBuzzController that gets fizz, buzz or fizzbuzz
 */
@WebMvcTest(FizzBuzzController.class)
public class GetFizzBuzzTest extends AbstractTestController {

    @MockBean
    private FizzBuzzService fizzBuzzService;

    private FizzBuzzModel fizzBuzzModel;
    private String entry;

    @BeforeEach
    public void initialize() {
        entry = Faker.instance().number().digit();
        fizzBuzzModel = FizzBuzzModel.builder().response(
            List.of(FizzBuzzData.builder().entry(Integer.parseInt(entry)).result("fizz").build())
        ).build();
    }

    @Test
    @DisplayName("should Return Response When Entry Is Number")
    void shouldReturnResponseWhenEntryIsNumber() throws Exception {
        doRequest()
            .andExpect(status().isOk());
    }

    private ResultActions doRequest() throws Exception {
        doReturn(fizzBuzzModel)
            .when(fizzBuzzService).getFizzBuzz(anyList());

        return getMockMvc()
            .perform(
                get("/fizzbuzz").queryParam("entry", entry)
            );
    }

}
