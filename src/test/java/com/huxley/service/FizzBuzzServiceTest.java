package com.huxley.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.huxley.controller.response.FizzBuzzModel;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * Integration test
 */
@SpringBootTest
public class FizzBuzzServiceTest {

    @Autowired
    private FizzBuzzService fizzBuzzService;

    @Nested
    @DisplayName("GetFizzBuzz")
    class GetFizzBuzz {

        @Test
        @DisplayName("should Return Single Response When Entry Is Passed")
        void shouldReturnSingleResponseWhenEntryIsPassed() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(3));
            assertEquals(fizzBuzzModel.getResponse().size(), 1);
        }

        @Test
        @DisplayName("should Return Multiple Responses When Entry Is Not Passed")
        void shouldReturnMultipleResponseWhenEntryIsNotPassed() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(null);
            assertEquals(fizzBuzzModel.getResponse().size(), 100);
        }

        @Test
        @DisplayName("should Return Fizz When Multiple Of 3 Is Given")
        void shouldReturnFizzWhenMultipleOf3IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(3));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("fizz"));
        }

        @Test
        @DisplayName("should Return Buzz When Multiple Of 5 Is Given")
        void shouldReturnBuzzWhenMultipleOf5IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(5));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("buzz"));
        }

        @Test
        @DisplayName("should Return Buzz When Multiple Of 7 Is Given")
        void shouldReturnBuzzWhenMultipleOf7IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(7));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("Bazz"));
        }

        @Test
        @DisplayName("should Return FizzBuzz When Multiple Of 3 And 5 Is Given")
        void shouldReturnFizzBuzzWhenMultipleOf3And5IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(15));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("fizzbuzz"));
        }

        @Test
        @DisplayName("should Return FizzBuzzBazz When Multiple Of 3 And 7 Is Given")
        void shouldReturnFizzBuzzBazzWhenMultipleOf3And7IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(21));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("FizzBazz"));
        }

        @Test
        @DisplayName("should Return FizzBuzzBazz When Multiple Of 5 And 7 Is Given")
        void shouldReturnFizzBuzzBazzWhenMultipleOf5And7IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(35));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("BuzzBazz"));
        }

        @Test
        @DisplayName("should Return FizzBuzzBazz When Multiple Of 3 And 5 And 7 Is Given")
        void shouldReturnFizzBuzzBazzWhenMultipleOf3And5And7IsGiven() {
            FizzBuzzModel fizzBuzzModel = getFizzBuzz(List.of(105));
            assertThat(fizzBuzzModel.getResponse().get(0).getResult(), is("FizzBuzzBazz"));
        }

        private FizzBuzzModel getFizzBuzz(List<Integer> entries) {
            return fizzBuzzService.getFizzBuzz(entries);
        }

    }

}
