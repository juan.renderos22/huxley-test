package com.huxley.controller;

import com.huxley.controller.response.FizzBuzzModel;
import com.huxley.service.FizzBuzzService;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Endpoint that expose the FunnyNames resources.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/fizzbuzz")
public class FizzBuzzController {

    @NonNull
    private final FizzBuzzService fizzBuzzService;

    @GetMapping
    public FizzBuzzModel getFizzBuzz(
        @RequestParam(value = "entry", required = false) final List<Integer> entries) {
        return fizzBuzzService.getFizzBuzz(entries);
    }

}


