package com.huxley.controller.response;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class FizzBuzzData {

    private Integer entry;
    private String result;

}
