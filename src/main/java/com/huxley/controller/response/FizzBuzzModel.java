package com.huxley.controller.response;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Model object to represent funny name information.
 */
@Getter
@Setter
@Builder(toBuilder = true)
public class FizzBuzzModel {

    private List<FizzBuzzData> response;

}
