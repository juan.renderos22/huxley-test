package com.huxley.service;

import com.huxley.controller.response.FizzBuzzModel;
import java.util.List;

/**
 * Business Logic to expose funnyName information.
 */
public interface FizzBuzzService {

    /**
     * Generate an array output for the clasic FizzBuzz game.
     *
     * @param entries list of Integer
     * @return FizzBuzzModel object model
     */
    FizzBuzzModel getFizzBuzz(final List<Integer> entries);

}
