package com.huxley.service;

public interface Multipleable {

    /**
     * Execute the chain of responsibility design pattern.
     *
     * @param number an entry number
     * @return String FizzBuzz game
     */
    String execute(final Integer number);

}
