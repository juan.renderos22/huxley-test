package com.huxley.service.implementation;

import com.huxley.controller.response.FizzBuzzData;
import com.huxley.controller.response.FizzBuzzModel;
import com.huxley.service.FizzBuzzService;
import com.huxley.service.MultipleChainService;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


@Service
@RequiredArgsConstructor
public class FizzBuzzServiceImpl implements FizzBuzzService {

    @NonNull
    private final MultipleChainService multipleChainService;

    public FizzBuzzModel getFizzBuzz(final List<Integer> entries) {
        return getFizzBuzzModel(entries);
    }

    private FizzBuzzModel getFizzBuzzModel(final List<Integer> entries) {

        List<FizzBuzzData> response;

        if (CollectionUtils.isEmpty(entries)) {
            response = getFizzBuzzFrom1To100();
        } else {
            response = getFizzBuzzFromList(entries);
        }

        return FizzBuzzModel
            .builder()
            .response(response)
            .build();
    }

    private List<FizzBuzzData> getFizzBuzzFromList(final List<Integer> entries) {
        List<FizzBuzzData> response = new ArrayList<>();
        for (Integer entry : entries) {
            if (entry != null) {
                response.add(
                    FizzBuzzData.builder().entry(entry).result(getFizzBuzzText(entry)).build()
                );
            }
        }

        return response;
    }

    private List<FizzBuzzData> getFizzBuzzFrom1To100() {
        List<FizzBuzzData> response = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            response.add(
                FizzBuzzData.builder().entry(i).result(getFizzBuzzText(i)).build()
            );
        }

        return response;
    }

    private String getFizzBuzzText(final Integer entry) {
        return multipleChainService.execute(entry);
    }

}
