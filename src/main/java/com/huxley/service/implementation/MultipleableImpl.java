package com.huxley.service.implementation;

import com.huxley.service.Multipleable;
import com.huxley.service.chain.MultipleChainProcessor;

public abstract class MultipleableImpl implements Multipleable {

    protected abstract MultipleChainProcessor getMultipleChainProcessor();

    @Override
    public String execute(final Integer number) {
        return getMultipleChainProcessor().perform(number);
    }

}
