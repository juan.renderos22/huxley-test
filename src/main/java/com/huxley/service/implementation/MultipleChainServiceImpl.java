package com.huxley.service.implementation;

import com.huxley.service.MultipleChainService;
import com.huxley.service.chain.MultipleChainProcessor;
import com.huxley.service.chain.strategies.MultipleOf3And5And7Strategy;
import com.huxley.service.chain.strategies.MultipleOf3And5Strategy;
import com.huxley.service.chain.strategies.MultipleOf3And7Strategy;
import com.huxley.service.chain.strategies.MultipleOf3Strategy;
import com.huxley.service.chain.strategies.MultipleOf5And7Strategy;
import com.huxley.service.chain.strategies.MultipleOf5Strategy;
import com.huxley.service.chain.strategies.MultipleOf7Strategy;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MultipleChainServiceImpl extends MultipleableImpl implements MultipleChainService {

    @Override
    protected MultipleChainProcessor getMultipleChainProcessor() {

        final MultipleChainProcessor chain = new MultipleOf3And5And7Strategy();
        chain.linkWith(new MultipleOf5And7Strategy())
            .linkWith(new MultipleOf3And7Strategy())
            .linkWith(new MultipleOf3And5Strategy())
            .linkWith(new MultipleOf7Strategy())
            .linkWith(new MultipleOf5Strategy())
            .linkWith(new MultipleOf3Strategy());

        return chain;
    }

}
