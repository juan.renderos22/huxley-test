package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf3And5Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 3 == 0 && number % 5 == 0) {
            return "fizzbuzz";
        }

        return checkNext(number);
    }
}
