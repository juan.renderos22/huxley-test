package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf5Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 5 == 0) {
            return "buzz";
        }

        return checkNext(number);
    }
    
}
