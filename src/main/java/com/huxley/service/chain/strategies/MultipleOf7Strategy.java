package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf7Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 7 == 0) {
            return "Bazz";
        }

        return checkNext(number);
    }
    
}
