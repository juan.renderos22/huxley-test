package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf3And7Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 3 == 0 && number % 7 == 0) {
            return "FizzBazz";
        }

        return checkNext(number);
    }
    
}
