package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf5And7Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 5 == 0 && number % 7 == 0) {
            return "BuzzBazz";
        }

        return checkNext(number);
    }

}
