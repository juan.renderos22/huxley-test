package com.huxley.service.chain.strategies;

import com.huxley.service.chain.MultipleChainProcessor;

public class MultipleOf3Strategy extends MultipleChainProcessor {

    @Override
    public String perform(final Integer number) {

        if (number % 3 == 0) {
            return "fizz";
        }

        return checkNext(number);
    }

}
