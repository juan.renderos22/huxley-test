package com.huxley.service.chain;

public abstract class MultipleChainProcessor {

    private MultipleChainProcessor next;


    public MultipleChainProcessor linkWith(final MultipleChainProcessor next) {
        this.next = next;
        return next;
    }

    public abstract String perform(final Integer number);


    public String checkNext(final Integer number) {
        if (next == null) {
            return number.toString();
        }
        return next.perform(number);
    }
}
