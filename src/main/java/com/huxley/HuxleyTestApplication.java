package com.huxley;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HuxleyTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(HuxleyTestApplication.class);
    }

}
