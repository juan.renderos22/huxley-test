package com.huxley.configuration;

import com.huxley.configuration.error.ApiError;
import com.huxley.configuration.error.ApiErrorWrapper;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Setups a Controller Advice to handle errors
 */
@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({Exception.class})
    protected ResponseEntity<Object> handleHttpClientError(final NumberFormatException ex,
                                                           final WebRequest request) {
        return this.handleExceptionInternal(ex, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, HttpHeaders headers,
                                                             HttpStatus status,
                                                             WebRequest request) {
        return this.handleExceptionInternal(ex, null, headers, status, request);
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, @Nullable Object body,
                                                             HttpHeaders headers, HttpStatus status,
                                                             WebRequest request) {
        if (Objects.isNull(body)) {
            ApiErrorWrapper apiErrors = this.message(status, ex);
            return new ResponseEntity(apiErrors, headers, status);
        } else {
            return new ResponseEntity(body, headers, status);
        }
    }

    protected ApiErrorWrapper message(final HttpStatus httpStatus, final Exception ex) {
        return this.message(this.buildApiError(httpStatus, ex));
    }

    protected ApiErrorWrapper message(final ApiError error) {
        ApiErrorWrapper errors = new ApiErrorWrapper();
        errors.addApiError(error);
        return errors;
    }

    private ApiError buildApiError(final HttpStatus httpStatus, final Exception ex) {
        String typeException = ex.getClass().getSimpleName();
        String description =
            StringUtils.defaultIfBlank(ex.getMessage(), ex.getClass().getSimpleName());
        String source = "base";
        if (this.isMissingRequestParameterException(ex)) {
            MissingServletRequestParameterException missingParamEx =
                (MissingServletRequestParameterException) ex;
            source = missingParamEx.getParameterName();
        } else if (this.isMissingPathVariableException(ex)) {
            MissingPathVariableException missingPathEx = (MissingPathVariableException) ex;
            source = missingPathEx.getVariableName();
        }

        return ApiError.builder().status(httpStatus.value()).type(typeException)
            .title(httpStatus.getReasonPhrase()).description(description).source(source).build();
    }

    private boolean isMissingPathVariableException(final Exception ex) {
        return ex instanceof MissingPathVariableException;
    }

    private boolean isMissingRequestParameterException(final Exception ex) {
        return ex instanceof MissingServletRequestParameterException;
    }

}
